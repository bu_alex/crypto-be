<div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        <?php
        foreach ( $docs as $key => $category )
        {
            echo '<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">+ ' . $key . '</a>';
            foreach ( $category as $item )
            {
                $unique = strtolower( str_replace( ' ', '-', $item->name ) );
                echo '<a class="nav-link" href="#v-pills-' . $unique . '" id="v-pills-' . $unique . '-tab" data-toggle="pill" role="tab" aria-controls="v-pills-' . $unique . '" aria-selected="true">' . $item->name . '</a>';
            }
        }
        ?>
        <hr>
        <a href="/doc/add" class="nav-link"> Add a new document </a>
    </div>
</div>