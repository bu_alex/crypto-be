<?php
/**
 * Created by PhpStorm.
 * User: bursak
 * Date: 5/19/19
 * Time: 01:12
 */
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );
?>
<form action="/doc/save" method="POST">
    <div class="form-group">
        <label for="fieldName">Name of document</label>
        <input type="text" name="name" class="form-control" id="fieldName" aria-describedby="fieldName"
               placeholder="Enter name of document" value="<?php echo $item->name; ?>">
    </div>
    <div class="form-group">
        <label for="fieldDescriptions">Descriptions</label>
        <textarea class="form-control" name="description" id="fieldDescriptions" placeholder="Enter description"
                  rows="3"><?php echo $item->description; ?></textarea>
    </div>
    <div class="form-group">
        <label for="fieldCategory">Category</label>
        <select class="form-control" id="fieldCategory" name="category">
            <?php
            foreach ( $categories as $key => $category )
            {
                echo "<option value='" . $key . "' " . ( $key == $item->category ? 'selected' : '' ) . ">" . $category . "</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="fieldUrl">URL</label>
        <input type="text" name="url" class="form-control" id="fieldUrl" aria-describedby="fieldUrl"
               placeholder="Enter URL" value="<?php echo $item->url; ?>">
    </div>
    <div class="form-group">
        <label for="fieldMethod">Category</label>
        <select class="form-control" id="fieldMethod" name="method">
            <?php
            foreach ( $list_method as $method )
            {
                echo "<option value='" . $method . "' " . ( $method == $item->method ? 'selected' : '' ) . ">" . $method . "</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="fieldURLparams">URL params</label>
        <textarea class="form-control" name="url_params" id="fieldURLparams" placeholder="Enter the URL params"
                  rows="3"><?php echo $item->url_params; ?></textarea>
    </div>
    <div class="form-group">
        <label for="fieldData">Data params</label>
        <textarea name="data" class="form-control" id="fieldData" aria-describedby="fieldData"
                  placeholder="Enter the Data params" rows="3"><?php echo $item->data; ?></textarea>
    </div>
    <div class="form-group">
        <label for="fieldSuccess">Success responce</label>
        <textarea name="success" class="form-control" id="fieldSuccess" aria-describedby="fieldSuccess"
                  placeholder="Enter success responce" rows="3"><?php echo $item->success; ?></textarea>
    </div>
    <div class="form-group">
        <label for="fieldUnsuccess">Unsuccess responce</label>
        <textarea name="unsuccess" class="form-control" id="fieldUnsuccess" aria-describedby="fieldUnsuccess"
                  placeholder="Enter unsuccess responce" rows="3"><?php echo $item->unsuccess; ?></textarea>
    </div>
    <div class="form-group">
        <label for="fieldSample">Sample</label>
        <textarea name="sample" class="form-control" id="fieldSample" aria-describedby="fieldSample"
                  placeholder="Enter sample" rows="3"><?php echo $item->sample; ?></textarea>
    </div>
    <div class="form-group">
        <label for="fieldNotes">Notes</label>
        <textarea name="notes" class="form-control" id="fieldNotes" aria-describedby="fieldNotes"
                  placeholder="Enter notes" rows="3"><?php echo $item->notes; ?></textarea>
    </div>
    <?php
    if ( !empty( $item->id ) )
    {
        echo '<input type="hidden" name="id" class="form-control" id="fieldId" aria-describedby="fieldId" value="' . $item->id . '">';
    }
    ?>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>