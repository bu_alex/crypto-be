<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Simple API</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
    <style>
        li.L0, li.L1, li.L2, li.L3,
        li.L5, li.L6, li.L7, li.L8 {
            list-style-type: decimal !important;
        }
    </style>

<script>
$( document ).ready(function() {
    $( document ).on( 'click', '.confirm_delete', function () {
        return confirm( 'Are you sure that you want to remove this item?' );
    } )
} );
</script>

</head>
<body>

<h1>Simple API for Crypto-endpoint</h1>

<div class="row">
    <?php $this->load->view('component/navigation'); ?>
    <div class="col-9">
        <div class="tab-content" id="v-pills-tabContent">

            <?php

            foreach ( $docs as $key => $category )
            {
                foreach ( $category as $item )
                {
                    $unique = strtolower( str_replace( ' ', '-', $item->name ) );
                    echo <<<TEMPLATE_HTML
            <div class="tab-pane fade" id="v-pills-{$unique}" role="tabpanel" aria-labelledby="v-pills-{$unique}-tab">
                <h2 style="display: inline-block;">{$item->name} </h2> | <a href="/doc/update/{$item->id}"> Edit </a> | <form method="POST" action="/doc/save" style="display: inline-block;"><input type="hidden" name="id" value="{$item->id}" /><button type="submit" class="confirm_delete"> Delete </button></form>
                <p>{$item->description}</p>
                <ul>
                    <li>
                        <h3>URL</h3>
                        <p>{$item->url}</p>
                    </li>
                    <li>
                        <h3>Method</h3>
                        <p>{$item->method}</p>
                    </li>
                    <li>
                        <h3>URL Params</h3>
                        <p>{$item->url_params}</p>
                    </li>
                    <li>
                        <h3>Data</h3>
                        <p>{$item->data}</p>
                    </li>
                    <li>
                        <h3>Success Response</h3>
                        <p>Content:</p>
                        <pre class="prettyprint linenums">
                            {$item->success}
                        </pre>
                    </li>
                    <li>
                        <h3>Unsuccess Response</h3>
                        <p>Content:</p>
                        <pre class="prettyprint linenums">
                            {$item->unsuccess}
                        </pre>
                    </li>
                    <li>
                        <h3>Sample Call</h3>
                        <p>{$item->sample}</p>
                    </li>
                    <li>
                        <h3>Notes</h3>
                        <p>{$item->notes}</p>
                    </li>
                </ul>
            </div>
TEMPLATE_HTML;
                }
            }

            ?>

        </div>
    </div>
</div>

</body>
</html>