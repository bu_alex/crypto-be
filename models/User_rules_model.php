<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_rules_model extends MY_Model {

    protected $_table_name = 'user_rules';

    public $user_id;
    public $status;
    public $coin_id_1;
    public $coin_id_2;
    public $inequality;
    public $order;
    public $date_start;
    public $date_end;
    public $date_create;

    function __construct () {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_new() {
        $result = new stdClass();
        $result->user_id = '';
        $result->status = 1;
        $result->coin_id_1 = '';
        $result->coin_id_2 = '';
        $result->inequality = '';
        $result->order = '';
        $result->date_start = '';
        $result->date_end = '';
        $result->date_create = '';
        return $result;
    }

    public function get_all()
    {
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_by_id( $id )
    {
        $this->db->where($this->_table_name.'.id', $id);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_my( $user_id )
    {
        $this->db->where($this->_table_name.'.user_id', $user_id);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function add( $item )
    {
        $this->user_id = $item->user_id;
        $this->status = $item->status ? 1 : 0;
        $this->coin_id_1 = $item->coin_id_1;
        $this->coin_id_2 = $item->coin_id_2;
        $this->inequality = $item->inequality;
        $this->order = $item->order;
        $this->date_start = $item->date_start ?: date( self::DATE_TIME_FORMAT );
        $this->date_end = $item->date_end ?: date( self::DATE_TIME_FORMAT, strtotime("+1 year", time()));
        $this->date_create = date( self::DATE_TIME_FORMAT );

        $this->db->insert( $this->_table_name, $this);
        return $this->db->insert_id();
    }

    public function update( $item )
    {
        $this->id = $item->id;
        $this->user_id = $item->user_id;
        $this->status = $item->status ? 1 : 0;
        $this->coin_id_1 = $item->coin_id_1;
        $this->coin_id_2 = $item->coin_id_2;
        $this->inequality = $item->inequality;
        $this->order = $item->order;
        $this->date_start = $item->date_start;
        $this->date_end = $item->date_end;

        $this->db->update( $this->_table_name, $this, [ 'id' => $this->id ] );
    }

}