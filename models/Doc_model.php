<?php
/**
 * Created by PhpStorm.
 * User: bursak
 * Date: 5/18/19
 * Time: 23:13
 */
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Doc_model extends MY_Model
{
    protected $_table_name = 'doc';

    public $category;
    public $name;
    public $description;
    public $url;
    public $method;
    public $url_params;
    public $data;
    public $success;
    public $unsuccess;
    public $sample;
    public $notes;

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_new()
    {
        $result              = new stdClass();
        $result->category    = '';
        $result->name        = '';
        $result->description = '';
        $result->url         = '';
        $result->method      = '';
        $result->url_params  = '';
        $result->data        = '';
        $result->success     = '';
        $result->unsuccess   = '';
        $result->sample      = '';
        $result->notes       = '';

        return $result;
    }

    public function get_name_by_id( $id )
    {
        $this->db->select( 'name' );
        $this->db->where( $this->_table_name . '.id', $id );
        $result = $this->db->get( $this->_table_name )->result();

        return !!$result[0] ? $result[0]->name : '';
    }

    public function get_by_id( $id )
    {
        $this->db->where( $this->_table_name . '.id', $id );
        $result = $this->db->get( $this->_table_name )->result();

        return !!$result[0] ? $result[0] : [];
    }

    public function get()
    {
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function add( $item )
    {
        $data = $this->sync_data( $item );
        $this->db->insert( $this->_table_name, $data );

        return $this->db->insert_id();
    }

    public function update( $item )
    {
        $data = $this->sync_data( $item );
        $this->db->update( $this->_table_name, $data, [ 'id' => $item['id'] ] );

        return $item['id'];
    }

    protected function sync_data( $item )
    {
        $result              = $this->get_new();
        $result->category    = $item['category'];
        $result->name        = $item['name'];
        $result->description = $item['description'];
        $result->url         = $item['url'];
        $result->method      = $item['method'];
        $result->url_params  = $item['url_params'];
        $result->data        = $item['data'];
        $result->success     = $item['success'];
        $result->unsuccess   = $item['unsuccess'];
        $result->sample      = $item['sample'];
        $result->notes       = $item['notes'];

        return $result;
    }

}

