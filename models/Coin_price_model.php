<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coin_price_model extends MY_Model {

    protected $_table_name = 'coin_price';

    public $coin_id;
    public $price;
    public $date_create;

    function __construct () {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_new() {
        $result = new stdClass();
        $result->coin_id = '';
        $result->price = '';
        $result->date_create = '';
        $result->volume_24h = '';
        $result->percent_change_1h = '';
        $result->percent_change_24h = '';
        $result->percent_change_7d = '';
        $result->market_cap = '';
        return $result;
    }

    public function get_all()
    {
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_all_by_coin_id( $coin_id ) {
        $this->db->where( 'coin_id', $coin_id );
        $this->db->order_by( "id", "asc" );
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_last_by_coin_id( $coin_id, $limit = 1 ) {
        $this->db->where( 'coin_id', $coin_id );
        $this->db->order_by( "id", "desc" );
        $this->db->limit( $limit );
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }
    
    public function add( $item )
    {
        $this->coin_id = $item['coin_id'];
        $this->price = $item['price'];
        $this->volume_24h = $item['volume_24h'];
        $this->percent_change_1h = $item['percent_change_1h'];
        $this->percent_change_24h = $item['percent_change_24h'];
        $this->percent_change_7d = $item['percent_change_7d'];
        $this->market_cap = $item['market_cap'];
        $this->date_create = date('Y-m-d H:i:s');

        $this->db->insert( $this->_table_name, $this);
        return $this->db->insert_id();
    }

}