<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Market_model extends MY_Model {

    protected $_table_name = 'market';

    public $name;
    public $addres;
    public $status;
    public $date_create;

    function __construct () {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_new() {
        $result = new stdClass();
        $result->name = '';
        $result->addres = '';
        $result->status = 1;
        $result->date_create = '';
        return $result;
    }

    public function get_all()
    {
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_by_id( $id )
    {
        $this->db->where($this->_table_name.'.id', $id);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_by_name( $name )
    {
        $this->db->where('name', $name);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function add()
    {
        $this->name     = $_GET['name'];
        $this->addres   = $_GET['addres'];
        $this->status   = $_GET['status'];
        $this->date_create = date('Y-m-d H:i:s');

        $this->db->insert( $this->_table_name, $this);
        return $this->db->insert_id();
    }

    public function update()
    {
        $this->name     = $_GET['name'];
        $this->addres   = $_GET['addres'];
        $this->status   = $_GET['status'];
        $this->date_create = date('Y-m-d H:i:s');

        $this->db->update( $this->_table_name, $this, [ 'name' => $this->name ] );
        return $this->name;
    }
}