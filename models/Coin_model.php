<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coin_model extends MY_Model {

    protected $_table_name = 'coin';

    public $symbol;
    public $name;
    public $status;

    function __construct () {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_new() {
        $result = new stdClass();
        $result->symbol = '';
        $result->name = '';
        $result->status = 1;
        return $result;
    }

    public function get_all()
    {
        $this->db->select($this->_table_name.'.*, coin_info.description');
        $this->db->join('coin_info', "coin_info.coin_id = {$this->_table_name}.id", 'left');
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }
    
    public function get_by_id( $id )
    {
        $this->db->select($this->_table_name.'.*, coin_info.description');
        $this->db->join('coin_info', "coin_info.coin_id = {$this->_table_name}.id", 'left');
        $this->db->where($this->_table_name.'.id', $id);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_by_symbol( $symbol )
    {
        $this->db->select($this->_table_name.'.*, coin_info.description');
        $this->db->join('coin_info', "coin_info.coin_id = {$this->_table_name}.id", 'left');
        $this->db->where('symbol', $symbol);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_symbol_by_id( $id )
    {
        $this->db->select( 'symbol' );
        $this->db->where( 'id', $id );
        $query = $this->db->get( $this->_table_name );
        $result = $query->result();
        return $result[0]->symbol;
    }

    public function get_id_by_symbol( $symbol )
    {
        $this->db->select( 'id' );
        $this->db->where( 'symbol', $symbol );
        $query = $this->db->get( $this->_table_name );
        $result = $query->result();
        return $result[0]->id;
    }

    public function add( $item )
    {
        $this->symbol   = $item['symbol'];
        $this->name     = $item['name'];
        $this->status   = $item['status'];

        $this->db->insert( $this->_table_name, $this);
        return $this->db->insert_id();
    }

    public function update( $item )
    {
        $this->symbol   = $item['symbol'];
        $this->name     = $item['name'];
        $this->status   = $item['status'];

        $this->db->update( $this->_table_name, $this, [ 'symbol' => $this->symbol ] );
        return $this->symbol;
    }

}