<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model {

    protected $_table_name = 'user';

    const PASSWORD_SALT = 'password_salt';

    public $uuid;
    public $username;
    public $password;
    public $status;
    public $date_create;

    function __construct () {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_new() {
        $result = new stdClass();
        $result->uuid = '';
        $result->username = '';
        $result->password = '';
        $result->status = 1;
        $result->date_create = '';
        return $result;
    }

    public function get_all()
    {
        $this->db->select( 'username, status, date_create' );
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_by_id( $id )
    {
        $this->db->select( 'username, status, date_create' );
        $this->db->where($this->_table_name.'.id', $id);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_id_by_uuid( $uuid )
    {
        $this->db->select( 'id, uuid' );
        $this->db->where('uuid', $uuid);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_by_uuid( $uuid )
    {
        $this->db->select( 'uuid, username, status, date_create' );
        $this->db->where('uuid', $uuid);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function get_by_username( $username )
    {
        $this->db->select( 'username, status, date_create' );
        $this->db->where('username', $username);
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }

    public function add( )
    {
        $this->uuid = $this->get_UUID();
        $this->status = $_POST['status'];
        $this->username = $_POST['username'];
        $this->password = $this->hash_password( $_POST['password'] );
        $this->date_create = date('Y-m-d H:i:s');

        $this->db->insert( $this->_table_name, $this);
        return $this->db->insert_id();
    }

    public function update( )
    {
        $this->uuid = $_POST['uuid'];
        $this->status = $_POST['status'];
        $this->username = $_POST['username'];
        $this->date_create = date('Y-m-d H:i:s');

        $this->db->update( $this->_table_name, $this, [ 'uuid' => $this->uuid ] );
        return $this->uuid;
    }

    public function update_password( $uuid )
    {
        $this->uuid = $uuid;
        $this->password = $this->hash_password( $_POST['password'] );
        $this->db->update( $this->_table_name, $this, [ 'uuid' => $this->uuid ] );
        return $this->uuid;
    }

    public function hash_password( $item )
    {
        return md5( $item . self::PASSWORD_SALT );
    }

    public function auth()
    {
        $this->username = $_POST['username'];
        $this->password = $this->hash_password( $_POST['password'] );
        $this->db->where('username', $this->username);
        $this->db->where('password', $this->password);
        return $this->get_once( $this->_table_name );
    }
}