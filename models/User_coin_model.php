<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_coin_model extends MY_Model {

    protected $_table_name = 'user_coin';

    public $user_id;
    public $coin_id;

    function __construct () {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_new() {
        $result = new stdClass();
        $result->user_id = '';
        $result->coin_id = '';
        return $result;
    }

    public function get_all_coin_by_user_id( $user_id ) 
    {
        $this->db->where( 'user_id', $user_id );
        $query = $this->db->get( $this->_table_name );
        return $query->result();
    }
    
    public function add( $item )
    {
        $this->user_id = $item['user_id'];
        $this->coin_id = $item['coin_id'];

        $this->db->insert( $this->_table_name, $this);
        return $this->db->insert_id();
    }

    public function delete( $item )
    {
        $this->user_id = $item['user_id'];
        $this->coin_id = $item['coin_id'];

        $this->db->where( 'user_id', $this->user_id );
        $this->db->where( 'coin_id', $this->coin_id );
        $this->db->delete( $this->_table_name );
    }

}