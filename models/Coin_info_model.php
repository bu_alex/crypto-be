<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coin_info_model extends MY_Model {

    protected $_table_name = 'coin_info';

    public $coin_id;
    public $description;

    function __construct () {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_new() {
        $result = new stdClass();
        $result->coin_id = '';
        $result->description = '';
        return $result;
    }

    public function add( $item )
    {
        $this->coin_id      = $item['coin_id'];
        $this->description  = $item['description'];

        $this->db->insert( $this->_table_name, $this);
    }

    public function update( $item )
    {
        $this->coin_id      = $item['coin_id'];
        $this->description  = $item['description'];

        $this->db->update( $this->_table_name, $this, [ 'coin_id' => $this->coin_id ] );
    }

    public function delete( $id )
    {
        $this->coin_id = $id;

        $this->db->where( 'coin_id', $this->coin_id );
        $this->db->delete( $this->_table_name );
    }

}