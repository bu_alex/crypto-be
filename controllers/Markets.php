<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Markets extends My_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('market_model');
    }

    public function index()
    {
        $result = $this->market_model->get_all();
		echo json_encode( $result );
    }

	public function get( $item )
	{
		$this->_display_item( $item );
    }

    public function add()
	{
		$market = $this->market_model->get_by_name( $_GET['name'] );
		if ( !empty( $market ) && $market[0]->id ) {
			$market_id = $market[0]->id;
		} else {
			$market_id = $this->market_model->add();
		}
		$this->_display_item( $market_id );
    }

	public function update()
	{
		$market_name = $this->market_model->update();
		$this->_display_item( $market_name );
	}

	public function delete( $item )
	{
		$market = $this->market_model->get_by_name( $item );
		if ( !empty( $market ) && $market[0]->id ) {
			$this->market_model->delete( $market[0]->id );
		}
		$this->_display( null, 'Delete item' );
	}
    
    private function _display_item( $item )
	{
		if ( is_numeric( $item ) ) {
            $result = $this->market_model->get_by_id( $item );	
        } else {
            $result = $this->market_model->get_by_name( $item );
        }
		$this->_display( $result );
	}

}