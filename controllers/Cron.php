<?php

/**
 * Description endpoint: https://coinmarketcap.com/api/documentation/v1/#operation/getV1CryptocurrencyQuotesLatest
 */

class Cron extends My_Controller {
    const PATH_ENDPOINT = 'https://pro-api.coinmarketcap.com/v1/';
    const LATEST_UPDATES = 'cryptocurrency/quotes/latest';

    const SECRET_KEY_FILE = 'config.ini';

    private $list_coins = [];
    private $list_data_coins = [];
    private $call_coins = [];
    private $tmp = [];

    private $count_success = 0;
    private $count_total = 0;

    private $secret_key = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('coin_model');
        $this->load->model('coin_price_model');
    }

    public function set_secret_key()
    {
        // root/config.ini <- should be created on server with SECRET KEY
        $this->secret_key = file_get_contents( BASEPATH . '../' . self::SECRET_KEY_FILE );
    }

    public function get_secret_key()
    {
        return $this->secret_key;
    }

    public function sync_coin()
    {
        $this->set_secret_key();
        $this->get_list_of_coins();
        $this->call_endpoint();
        $this->exec_result();
        $this->display();
    }

    public function call_endpoint()
    {
        $param = '?symbol=' . implode( ',', $this->list_coins ) ;
        $url = self::PATH_ENDPOINT . self::LATEST_UPDATES . $param;
        $this->load->library( 'apirest', [ 'url' => $url ] );
        $result = $this->apirest->call(
            array( 'X-CMC_PRO_API_KEY:' . $this->get_secret_key() )
        );
        $this->call_coins = json_decode( $result, true );
    }

    public function get_list_of_coins()
    {
        $coins = $this->coin_model->get_all();
        $this->list_coins = array_map( function ( $item ) {
            $this->list_data_coins[ $item->symbol ] = $item->id;
            return $item->symbol;
        }, $coins );
    }

    public function exec_result()
    {
        $this->tmp = array_map( function ( $item ) {
            if ( in_array($item['symbol'], $this->list_coins) ) {
                $coin_data = $item['quote']['USD'];
                $this->coin_price_model->add( [
                    'coin_id' => $this->list_data_coins[ $item['symbol'] ],
                    'price' => $coin_data['price'],
                    'volume_24h' => $coin_data['volume_24h'],
                    'percent_change_1h' => $coin_data['percent_change_1h'],
                    'percent_change_24h' => $coin_data['percent_change_24h'],
                    'percent_change_7d' => $coin_data['percent_change_7d'],
                    'market_cap' => $coin_data['market_cap']
                ] );
                $this->count_success++;
            }
            $this->count_total++;
            return $item;
        }, $this->call_coins[ 'data' ] );
    }

    public function display()
    {
        echo "Обработано: " . $this->count_total . ' монет!' . PHP_EOL;
        echo "Oбновлено: " . $this->count_success . ' монет!' . PHP_EOL;
    }

}