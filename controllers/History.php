<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends My_Controller {

    protected $expand = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('coin_model');
		$this->load->model('coin_info_model');
		$this->load->model('coin_price_model');
		if ( !empty( $_GET['expand'] ) ) {
			$this->expand = 1;
		}
    }
    
    public function index( $item )
	{
        $this->_display_item( $item );

    }
    
    private function _display_item( $item = null )
	{
		if ( is_numeric( $item ) ) {
			$result = $this->coin_model->get_by_id( $item );
		} else {
			$result = $this->coin_model->get_by_symbol( $item );
		}
		
		if ( !$result ) {
			throw new Exception('Nothing to find!');
		}

		$result = array_map( function ( $item ) {
			$price = $this->coin_price_model->get_last_by_coin_id( $item->id, 500 );
			if ( $this->expand ) {
				$item->coin_info = empty( $price ) ? null : [
					'volume_24h' => end($price)->volume_24h,
					'percent_change_1h' => end($price)->percent_change_1h,
					'percent_change_24h' => end($price)->percent_change_24h,
					'percent_change_7d' => end($price)->percent_change_7d,
					'market_cap' => end($price)->market_cap,
				];
            }
            $item->price = array_map( function ( $item ) {
                return $item->price;
            }, $price );
			return $item;
		}, $result );
		$this->_display( $result );
	}

}
