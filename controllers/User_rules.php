<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_rules extends My_Controller {

    const ORDER_0 = '<=';
    const ORDER_1 = '>=';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('coin_model');
        $this->load->model('user_coin_model');
        $this->load->model('user_rules_model','user_rules');
		$this->load->model('coin_price_model');
	}

	public function index( $uuid )
	{
        $userId = $this->_get_user_id( $uuid );
        $rules = $this->user_rules->get_my( $userId );
        $array = array_map( function ( $item ) use ( $uuid ) {
            return $this->_get_rule_item( $item, $uuid );
        }, $rules );
        $this->_display( $array );
    }

    public function add( )
    {
        $uuid = $_POST['uuid'];
        $user_id = $this->_get_user_id( $uuid );
        $new_rules = $this->user_rules->get_new();

        $coin_id_1 = $this->coin_model->get_id_by_symbol( $_POST[ 'coin_1' ] );
        if ( empty($_POST[ 'coin_2' ]) ) {
            $coin_id_2 = 'USDT';
        } else {
            $coin_id_2 = $this->coin_model->get_id_by_symbol( $_POST[ 'coin_2' ] );
        }

        $new_rules->user_id = $user_id;
        $new_rules->status = isset($_POST['status']) ? $_POST['status'] : $new_rules->status;
        $new_rules->coin_id_1 = $coin_id_1;
        $new_rules->coin_id_2 = $coin_id_2;
        $new_rules->inequality = $_POST['inequality'];
        $new_rules->order = $_POST['order'];
        $new_rules->date_start = isset($_POST['date_start']) ? $_POST['date_start'] : null;
        $new_rules->date_end = isset($_POST['date_end']) ? $_POST['date_end'] : null;

        $rules = $this->user_rules->add( $new_rules );
        $item = $this->user_rules->get_by_id( $rules );
        $this->_display( $this->_get_rule_item( $item[0], $uuid ) );
    }

    public function update( $id ) {
        try {
            $uuid = $_POST['uuid'];
            $user_id = $this->_get_user_id( $uuid );
            if ( $this->validate_rule( $user_id, $id ) !== true ) {
                throw new Exception('Invalid item ID!');
            }
            $new_rules = $this->user_rules->get_new();

            $coin_id_1 = $this->coin_model->get_id_by_symbol( $_POST[ 'coin_1' ] );
            if ( empty($_POST[ 'coin_2' ]) ) {
                $coin_id_2 = 'USDT';
            } else {
                $coin_id_2 = $this->coin_model->get_id_by_symbol( $_POST[ 'coin_2' ] );
            }

            $new_rules->id = $id;
            $new_rules->user_id = $user_id;
            $new_rules->status = isset($_POST['status']) ? $_POST['status'] : $new_rules->status;
            $new_rules->coin_id_1 = $coin_id_1;
            $new_rules->coin_id_2 = $coin_id_2;
            $new_rules->inequality = $_POST['inequality'];
            $new_rules->order = $_POST['order'];
            $new_rules->date_start = isset($_POST['date_start']) ? $_POST['date_start'] : null;
            $new_rules->date_end = isset($_POST['date_end']) ? $_POST['date_end'] : null;

            $rules = $this->user_rules->update( $new_rules );
            $item = $this->user_rules->get_by_id( $id );
            $this->_display( $this->_get_rule_item( $item[0], $uuid ) );
        } catch ( Exception $e ) {
            $this->_display( null, $e->getMessage() );
        }
    }

    public function delete( $id )
    {
        try {
            $user_id = $this->_get_user_id( $_POST['uuid'] );
            if ( $this->validate_rule( $user_id, $id ) !== true ) {
                throw new Exception('Invalid item ID!');
            }
            $this->user_rules->delete( $id );
            $this->_display( null, 'Delete item' );
        } catch ( Exception $e ) {
            $this->_display( null, $e->getMessage() );
        }
    }

    protected function validate_rule( $user_id, $itemId )
    {
        $item = $this->user_rules->get_by_id( $itemId );
        if ( empty($item) || $item[0]->user_id != $user_id ) {
            return false;
        }
        return true;
    }

    protected function _get_rule_item( $item, $uuid )
    {
        $coin_id_1 = $this->coin_model->get_symbol_by_id( $item->coin_id_1 );
        $coin_id_2 = $this->coin_model->get_symbol_by_id( $item->coin_id_2 );
        $curr_inequality = $this->get_inequality( $item->coin_id_1, $item->coin_id_2 );
        return [
          'id' => $item->id,
          'status' => $item->status,
          'coin_id_1' => $coin_id_1,
          'coin_id_2' => $coin_id_2,
          'inequality' => $item->inequality,
          'curr_inequality' => $curr_inequality,
          'order' => $item->order,
          'date_start' => $item->date_start,
          'date_end' => $item->date_end
        ];
    }
    
}