<?php
defined('BASEPATH') OR exit('No direct script access allowed');


// /* 01:43:09 LocalHost db_crypto */ ALTER TABLE `user` ADD `password` CHAR(64)  NULL  DEFAULT NULL  AFTER `username`;
// /* 01:45:09 LocalHost db_crypto */ ALTER TABLE `user` ADD INDEX `IndAuth` (`password`, `username`);


class Users extends My_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('coin_model');
		$this->load->model('user_coin_model');
		$this->load->model('coin_price_model');
	}

	public function index()
	{
		$this->_display_item( );
	}

	public function get( $item )
	{
		$this->_display_item( $item );
	}
	
	public function get_coin( $item )
	{
		$user_id = $this->_get_user_id( $item );
		$result = $this->user_coin_model->get_all_coin_by_user_id( $user_id );
		$result = array_map( function ( $item ) {
			$coin = $this->coin_model->get_by_id( $item->coin_id );
			$price = $this->coin_price_model->get_last_by_coin_id( $item->coin_id );
			$item->price = empty( $price ) ? null : $price[0]->price;
			return [
				'coin_id' => $item->coin_id,
				'coin_symbol' => $coin[0]->symbol,
				'coin_name' => $coin[0]->name,
				'coin_price' => $item->price
			];
		}, $result );
		$this->_display( $result );
	}

	public function auth()
	{
		$result = $this->user_model->auth( );
		if ( !!$result ) {
			$this->_display_item( $result->uuid );
		} else {
			$message = [ 'message' => 'User not found' ];
			$this->_display( $message );
		}
	}

	public function add()
	{
		$user = $this->_get_user_data( $_POST['username'] );
		if ( !empty( $user ) && $user[0]->id ) {
			$user_id = $user[0]->id;
		} else {
			$user_id = $this->user_model->add();
		}
		$this->_display_item( $user_id );
	}

	public function update( $uuid )
	{
		$user_uuid = $this->user_model->update( $uuid );
		$this->_display_item( $user_uuid );
	}
	
	public function delete( $uuid )
	{
		$message = [ 'message' => 'Incorrect UUID' ];
		if ( $this->_is_uuid( $uuid ) ) {
			$user = $this->user_model->get_by_uuid( $uuid );
			if ( !empty( $user ) && $user[0]->id ) {
				$this->user_model->delete( $user[0]->id );
			}
			$message = [ 'message' => 'Delete item' ];
			return;
		}
		$this->_display( $message );
	}

	private function _get_user_data( $item = null )
	{
		if ( empty( $item ) ) {
			$result = $this->user_model->get_all();
		} else if ( is_numeric( $item ) ) {
			$result = $this->user_model->get_by_id( $item );
		} else if ( $this->_is_uuid( $item ) ) {
			$result = $this->user_model->get_by_uuid( $item );
		} else {
			$result = $this->user_model->get_by_username( $item );
		}
		return $result;
	}

	private function _display_item( $item = null )
	{
		$result = $this->_get_user_data( $item );
		$this->_display( $result );
	}
}