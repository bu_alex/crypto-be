<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coins extends My_Controller {

	protected $expand = false;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('coin_model');
		$this->load->model('coin_info_model');
		$this->load->model('coin_price_model');
		if ( !empty( $_GET['expand'] ) ) {
			$this->expand = 1;
		}
	}

	public function index()
	{
		$this->_display_item( );
	}

	public function get( $item = null )
	{
		try {
			if ( $item ) {
				$this->_display_item( $item );
			} else {
				throw new Exception('The param should be set!');
			}
		} catch ( Exception $e ) {
			$this->_display( [], $e->getMessage() );
		}
	}

	public function add()
	{
		try {
			if ( empty($_POST['symbol']) ) {
				throw new Exception('Symbol should be set!');
			}
			$coin = $this->coin_model->get_by_symbol( $_POST['symbol'] );
			if ( !empty( $coin ) && $coin[0]->id ) {
				$coin_id = $coin[0]->id;
			} else {
				$coin_info = [
					'symbol' 		=> $_POST['symbol'],
					'name' 			=> $_POST['name'],
					'status' 		=> $_POST['status'],
					'description' 	=> $_POST['description']
				];
				$coin_id = $this->coin_model->add( $coin_info );
				$coin_info[ 'coin_id' ] = $coin_id;
				$coin_info = $this->coin_info_model->add( $coin_info );
			}
			$this->_display_item( $coin_id );
		} catch ( Exception $e ) {
			$this->_display( null, $e->getMessage() );
		}
	}

	public function update( $symbol = null )
	{
		try {
			if ( empty( $symbol ) ) {
				throw new Exception('Symbol should be set!');
			}
			$coin_info = [
				'symbol' 		=> $symbol,
				'name' 			=> $_POST['name'],
				'status' 		=> $_POST['status'],
				'description' 	=> $_POST['description']
			];
			$coin_symbol = $this->coin_model->update( $coin_info );
			$coin = $this->coin_model->get_by_symbol( $coin_symbol );
			$coin_info[ 'coin_id' ] = $coin[0]->id;
			$coin_info = $this->coin_info_model->update( $coin_info );
			$this->_display_item( $coin_symbol );
		} catch ( Exception $e ) {
			$this->_display( null, $e->getMessage() );
		}
	}

	public function delete( $item )
	{
		try {
			$coin = $this->coin_model->get_by_symbol( $item );
			if ( $coin[0]->id ) {
				$this->coin_model->delete( $coin[0]->id );
				$this->coin_info_model->delete( $coin[0]->id );
			}
			$this->_display( null, 'Delete item' );
		} catch ( Exception $e ) {
			$this->_display( null, $e->getMessage() );
		}
	}

	private function _display_item( $item = null )
	{
		if ( empty( $item ) ) {
			$result = $this->coin_model->get_all();
		} else  if ( is_numeric( $item ) ) {
			$result = $this->coin_model->get_by_id( $item );
		} else {
			$result = $this->coin_model->get_by_symbol( $item );
		}
		
		if ( !$result ) {
			throw new Exception('Nothing to find!');
		}

		$result = array_map( function ( $item ) {
			$price = $this->coin_price_model->get_last_by_coin_id( $item->id );
			if ( $this->expand ) {
				$item->coin_info = empty( $price ) ? null : [
					'volume_24h' => $price[0]->volume_24h,
					'percent_change_1h' => $price[0]->percent_change_1h,
					'percent_change_24h' => $price[0]->percent_change_24h,
					'percent_change_7d' => $price[0]->percent_change_7d,
					'market_cap' => $price[0]->market_cap,
				];
			}
			$item->price = empty( $price ) ? null : $price[0]->price;
			return $item;
		}, $result );
		$this->_display( $result );
	}
}
