<?php
/**
 * Created by PhpStorm.
 * User: bursak
 * Date: 5/18/19
 * Time: 15:30
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Doc extends My_Controller {

    public $data = '';
    const ACTION_ADD = 'add';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('doc_model');
    }

    public function index()
    {
        $this->data['categories'] = $this->_list_categories();
        $this->data['docs'] = $this->_refactore_code( $this->doc_model->get() );
    }

    public function add()
    {
        $data['title'] = 'Add a new document';
        $data['item'] = $this->doc_model->get_new();
        $data['list_categories'] = $this->_list_categories();
        $data['list_method'] = $this->_list_method();

        $data['categories'] = $this->_list_categories();
        $data['docs'] = $this->_refactore_code( $this->doc_model->get() );

        $this->load->view('doc_form', $data);
    }

    public function update( $id )
    {
        $data['title'] = 'Edit document';
        $data['item'] = $this->doc_model->get_by_id( $id );
        $data['list_categories'] = $this->_list_categories();
        $data['list_method'] = $this->_list_method();

        $data['categories'] = $this->_list_categories();
        $data['docs'] = $this->_refactore_code( $this->doc_model->get() );

        $this->load->view('doc_form', $data);
    }

    public function save()
    {
        try {
            if ( empty( $_POST ) ) {
                throw new Exception('The data could not be empty');
            }
            switch ( true ) {
                case (!isset( $_POST['id'] )):
                    $action = self::ACTION_ADD;
                break;
                case (!isset( $_POST['url'] )):
                    $action = self::ACTION_DELETE;
                break;
                default:
                    $action = self::ACTION_UPDATE;
            }

            if ( $action == self::ACTION_DELETE ) {
                $this->doc_model->delete( $_POST['id'] );
            }

            if ( $action == self::ACTION_UPDATE ) {
                $item = $_POST;
                $id = $this->doc_model->update( $item, $_POST['id'] );
            }

            if ( $action == self::ACTION_ADD ) {
                $item = $_POST;
                $id = $this->doc_model->add( $item );
            }

            $url_title_name = strtolower( str_replace( ' ', '-', $this->_get_name( $id ) ) );
            $redirect = '/#v-pill-' . $url_title_name;

        } catch ( Exception $e ) {
             var_dump( $e->getMessage() );
        }

        header('Location: ' . $redirect);
    }

    protected function _get_name( $id )
    {
        return $this->doc_model->get_name_by_id( $id );
    }

    protected function _list_method()
    {
        return [ 'POST', 'GET' ];
    }

}