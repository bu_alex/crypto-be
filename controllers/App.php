<?php
/**
 * Created by PhpStorm.
 * User: bursak
 * Date: 5/24/19
 * Time: 15:01
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends My_Controller
{
    public $data = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('application', $this->data);
    }
}