<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends My_Controller {
    public $data = array();

	public function __construct()
	{
		parent::__construct();
        $this->load->model('doc_model');
    }

    public function index()
    {
        $this->data['categories'] = $this->_list_categories();
        $this->data['docs'] = $this->_refactore_code( $this->doc_model->get() );
        $this->load->view('api_page', $this->data);
    }
}