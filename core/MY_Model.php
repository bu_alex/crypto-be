<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    function __construct () {
        // Call the CI_Model constructor
        $this->load->database();
    }

    public function get_UUID()
    {
        $query = $this->db->query( 'SELECT UUID() as uuid' );
        $result = $query->result();
        return $result[0]->uuid;
    }

    public function delete( $id )
    {
        $this->db->where( 'id', $id );
        $this->db->delete( $this->_table_name );
    }

    public function get_once( $table )
    {
        $query = $this->db->get( $table );
        if ( is_array( $query->result() ) && count( $query->result() ) ) {
            return $query->result()[0];
        }
        return false;
    }
    
}