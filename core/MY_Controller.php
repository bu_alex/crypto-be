<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {

    public function _display( $data = null, $message = null )
    {
        $array = [];
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');

        if ( $message && is_null($data) ) {
            $array = [
                "status" => 400,
                "success" => false,
                "response" => [
                    "message" => $message
                ]
            ];
        } else if ( $message ) {
            $array = [
                "status" => 200,
                "success" => true,
                "response" => [
                    "message" => $message
                ]
            ];
        } else {
            $array = [
                "status" => 200,
                "success" => true,
                "data" => $data
            ];
        }

        echo json_encode( $array );
    }

	protected function _is_uuid( $uuid )
	{
		return !!preg_match( '/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/', $uuid );
    }
    
    protected function _get_user( $uuid )
    {
        $user = $this->user_model->get_by_uuid( $uuid );
        if ( !empty( $user ) && $user[0]->id ) {
            return $user[0];
		} else {
			$this->_display( null, 'UUID is required!' );
			return;
        }
    }

    protected function _get_user_id( $uuid )
    {
        $user = $this->user_model->get_id_by_uuid( $uuid );
        if ( !empty( $user ) && $user[0]->id ) {
            return $user[0]->id;
        } else {
            $this->_display( null, 'UUID is required!' );
            return;
        }

    }

    protected function get_price_by_coin_id( $coin_id )
    {
        $detail_coin = $this->coin_price_model->get_last_by_coin_id( $coin_id );
        if ( is_array( $detail_coin ) && !empty( $detail_coin ) ) {
            return  $detail_coin[0]->price;
        }
        return 0;
    }

    protected function get_inequality( $coin_1, $coin_2 )
    {
        $price_coin_1 = $this->get_price_by_coin_id( $coin_1 );
        $price_coin_2 = $this->get_price_by_coin_id( $coin_2 );
        if ( $price_coin_2 && $price_coin_1 ) {
            return $price_coin_2/$price_coin_1;
        }
        return 0;
    }

    protected function _list_categories()
    {
        return [
          '1' => 'Coins',
          '2' => 'Markets',
          '3' => 'Users',
          '4' => 'User rules',
          '5' => 'History'
        ];
    }

    public function _refactore_code( $items )
    {
        $new_items = [];
        $categories = $this->_list_categories();

        foreach( $items as $key => $value ) {
            $new_items[ $categories[ $value->category ] ][] = $value;
        }
        return $new_items;
    }
}